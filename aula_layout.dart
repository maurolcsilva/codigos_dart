import 'package:flutter/material.dart';

void main() {
  runApp(MinhaApp());
}

class MinhaApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Minha App Layout",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: PaginaInicial(),
    );
  }
}

class PaginaInicial extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("App Layout"),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.yellow,
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            myBox(context, "Primeira Linha", Colors.blue),
            myBox(context, "Segunda Linha", Colors.orange),
            Text("Terceira Linha"),
          ],
        ),
      ),
    );
  }
}

Widget myBox(BuildContext context, String texto, Color cor) {
  return Container(
    width: MediaQuery.of(context).size.width,
    height: 60,
    color: cor,
    child: Align(
      alignment: Alignment.center,
      child: Text(texto),
    ),
  );
}
