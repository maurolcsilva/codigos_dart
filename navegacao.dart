import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.blue),
      debugShowCheckedModeBanner: false,
      home: Pagina01(),
    );
  }
}

class Pagina01 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Página 01")),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        margin: EdgeInsets.all(5),
        color: Colors.cyan,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_forward),
        onPressed: () {
          Professor prof = Professor("Mauro Lopes","Sistemas Distribuídos");
          Navigator.push(
            context, 
            MaterialPageRoute(builder: (context) => Pagina02(prof))
          );
        },
      ),
    );
  }
}

class Pagina02 extends StatelessWidget {
  Professor professor = Professor("","");
  
  Pagina02.limpo(){
    
  }
  
  Pagina02(Professor professor){
    this.professor = professor;
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Página 02")),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        margin: EdgeInsets.all(5),
        color: Colors.orange,
        child: Center(
          child: Text(professor.nome + " -" + professor.disciplina),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_back),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}


class Professor {
  String _nome = "";
  String _disciplina = "";
  
  Professor(String nome, String disciplina){
    this._nome = nome;
    this._disciplina = disciplina;
  }
  
  String get nome=>this._nome;
  String get disciplina=>this._disciplina;
  
  set nome(String nome)=>this._nome = nome;
  set disciplina(String disciplina)=>this._disciplina = disciplina;
  
}

