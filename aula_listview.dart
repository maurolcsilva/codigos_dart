import 'package:flutter/material.dart';

void main() {
  runApp(MinhaApp());
}

class MinhaApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: PaginaInicial(),
    );
  }
}

class PaginaInicial extends StatelessWidget {
  List<Professor> lista = [
    Professor("Mauro", "Tópicos 2"),
    Professor("Josenildo", "Ciência de Dados"),
    Professor("Carla Faria", "Análise II"),
    Professor("Eveline", "IHC"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Listas"),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        margin: EdgeInsets.all(5),
        color: Colors.white,
        child: ListView.builder(
          itemCount: lista.length,
          itemBuilder: (context,index){
            return ListTile(
              leading: CircleAvatar(
                child: Text("P"),
                backgroundColor: Colors.blue,
                foregroundColor: Colors.white,
              ),
              title: Text(lista[index].nome),
              subtitle: Text(lista[index].disciplina),
              onTap: (){
                print(lista[index].nome);
              },
            );
          },
        ),
      ),
    );
  }
}

class Professor {
  String _nome = "";
  String _disciplina = "";

  Professor(String nome, String disciplina) {
    this._nome = nome;
    this._disciplina = disciplina;
  }

  String get nome => this._nome;
  String get disciplina => this._disciplina;

  set nome(String nome) => this._nome = nome;
  set disciplina(String disciplina) => this._disciplina = disciplina;
}
